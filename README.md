# Desafio NESS Backend

## Getting started 👋

Olá!

Bem vindo ao desafio para a vaga de Desenvolvedor Backend, se você chegou até aqui, significa que escolhemos você para participar do nosso teste seletivo.

Neste repositório, você encontrará um projeto em Laravel (PHP). Se tiver familiaridade, temos também os arquivos para executar um ambiente usando docker, ou até mesmo lando.

Fique a vontade, explore o código e quando se sentir confortável, comece o desafio.

## Funcionalidades

Nós temos as seguintes funcionalidades já implementadas:

-   **Criação de usuário**: Apenas um endpoint para cadastro de novos usuários.
-   **Login**: Enpoint para o login. Retorna um token.
-   **Listagem das informações da escola cadastrada**: Retorna a escola que o usuário é associado.

## Desafio

Abaixo você encontrará uma série de tarefas que devem ser completadas.

Nosso contexto é um aplicativo educacional, e estamos em fase de desenvolvimento ainda. Como foi possível verificar, temos poucas funcionalidades prontas, por isto, precisamos de você para auxiliar na criação das demais funcionalidades.

> 🛑️ Antes de iniciar o teste, leia com atenção as intruções.

### Instruções

Abaixo estão listadas as tarefas/funcionalidades que precisamos que sejam desenvolvidas, e para cada uma, antes de iniciar, faça um commit preenchendo o campo sobre a estimativa de tempo. Após isso, mão na massa, e ao final, novamente um novo commit para indicar que finalizou a _task_.

Não exclua ou altere nenhum dos arquivos existentes, exceto quando muito necessário e exista um motivo para tal.

Estaremos analisando os seguintes aspectos:

-   se a funcionalidade funciona e corresponde ao que foi pedido
-   organização e estrutura do código
-   testes unitários
-   estrutura e organização de _commits_

#### Professor e escola

Notamos que precisamos arrumar uma coisa fundamental na nossa solução, a relação entre professor e escola. Na atual implementação, ele pertence apenas a uma escola, mas conversando com os professores, entendemos melhor o contexto. Um professor pode pertencer a mais de uma escola.

É importante observar que o banco de dados já está em produção. Portanto, você deve criar novas migrações para esta tarefa em vez de modificar as existentes.

Também é importante ressaltar que existe o endpoint: `/api/escola` que retorna a escola que o professor esta ligado. Você deve alterar o nome da rota para `/api/escolas` e o novo retorno deverá ser uma lista com as escolas que o professor possui vinculo.

Efetue as alterações necessárias, e certifique-se de adicionar testes.

✋ **Antes de começar**

> Atualize a linha abaixo neste `README.md` adicionando a estimativa de tempo para completar a tarefa.

Tempo estimado: [ 3h30m ]

> Depois de atualizar a estimativa e antes de começar a codificar, confirme suas alterações usando o seguinte comando:
> `git add README.md && git commit -m "Estimativa tarefa 1" && git push`

🏁 **Assim que completar a tarefa**

> Após implementar as alterações necessárias, suba utilizando os seguintes comandos:
> `git add -A && git commit -m ":alarm_clock: Alterando relação professor escola" && git push`

#### Alunos e a escola

Toda escola, além de professores, precisa de alunos. Crie as funcionalidades para gerenciar (famoso CRUD) os alunos. Lembrando que um aluno possui vínculo com apenas uma escola. Os campos necessários para o cadastro dos alunos são: nome, data de nascimento, nome do responsável.

Lembrando que nosso banco de dados já está em produção. Portanto, você deve criar novas migrações para esta tarefa em vez de modificar as existentes.

Para o gerenciamento de aluno, utilize o seguinte endpoint: `/api/alunos`.
Além disso, crie um endpoint: `/api/escola/{escola}/alunos` que retorna todos os alunos de uma determinada escola.

Efetue as alterações necessárias, e certifique-se de adicionar os testes necessários.

✋ **Antes de começar**

> Atualize a linha abaixo neste `README.md` adicionando a estimativa de tempo para completar a tarefa.

Tempo estimado: [ 4h ]

> Depois de atualizar a estimativa e antes de começar a codificar, confirme suas alterações usando o seguinte comando:
> `git add README.md && git commit -m "Estimativa tarefa 2" && git push`

🏁 **Assim que completar a tarefa**

> Após implementar as alterações necessárias, suba utilizando os seguintes comandos:
> `git add -A && git commit -m ":alarm_clock: Lugar de aluno é na escola" && git push`

#### Cada aluno no seu quadrado

Agora que temos os alunos, notamos que é preciso organiza-los e dividi-los em turmas. Uma turma precisa de um professor responsável, além dos alunos. não preciso dizer que a turma é referente apenas a uma escola. E toda turma tem um código de identificação, por exemplo, 1TA01 ou 1TA02.

Novamente, você deve criar novas migrações para esta tarefa em vez de modificar as existentes.

Para o gerenciamento das turmas e alunos, utilize os seguintes endpoints:

-   `/api/turmas` : para gerenciar as turmas - CRUD. Quando for criar a turma, a _request_ possui o seguinte payload:

    ```json
    {
        "codigo": "1TA02",
        "responsavel": 1,
        "alunos": [1, 2, 3, 4, 5]
    }
    ```

-   `api/turmas/{turma}/alunos`: para listar os alunos da turma

Efetue as alterações necessárias, e certifique-se de adicionar os testes necessários.

✋ **Antes de começar**

> Atualize a linha abaixo neste `README.md` adicionando a estimativa de tempo para completar a tarefa.

Tempo estimado: [ 3h ]

> Depois de atualizar a estimativa e antes de começar a codificar, confirme suas alterações usando o seguinte comando:
> `git add README.md && git commit -m "Estimativa tarefa 3" && git push`

🏁 **Assim que completar a tarefa**

> Após implementar as alterações necessárias, suba utilizando os seguintes comandos:
> `git add -A && git commit -m ":alarm_clock: Cada aluno no seu quadrado" && git push`

## Notas ao avaliador

Documentei a api utilizando swagger na seguinte url: http://localhost/api/documentation. Sei que não tinha necessidade mas como vou deixar esse projeto meu repositório tratei de ter um carinho especial por ele;

Criei uma pasta chamada "\_interview_assets" e adicionei arquivos pertinentes lá, como o dump da database e as rotas do Insomnia.

Criei um command personalizado pra facilitar as migrations e os seeds, basta rodar: `php artisan custom:run`

Ao implementar o CRUD, adicionei soft delete, seguindo o padrão da tabela 'school';

Notei ao chegar no terceiro exercício que o payload informado estava em português e assim o fiz. Não alterei o segundo exercício pois a descrição me levou a crer que eu poderia escrevê-lo em inglês. Já no terceiro, me pareceu ser um requisito obrigatório, e então codifiquei o payload em português como solicitado;

Ao implementar o terceiro exercício, escrevi uma rotina para tornar o código uma chave única sem tornar a coluna do banco unique (visto que foi utilizado soft delete);

Realizei validações adicionais com campo de datas pois utilizei um plugin de mockup no Insomnia que insere com o tipo string;

Qualquer dúvida estarei a disposição e espero poder contribuir com vocês em breve!
